﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace RegexApp
{
    
    internal class Program
    {
        private const string Url = "https://www.chess.com/stats/puzzles/fformula?type=puzzle-rush#recent";
        public static void Main(string[] args)
        {
            //при загрузке через WebClient не отрабатывают скрипты,
            //поэтому страница была сохранена в файл и читается (GetDataFromFile) из него
            //string data = GetDataFromUrl(Url);
            string data = GetDataFromFile();

            string pattern = "(?<=v5-list-item-label\">)(.+?)(?=</span>)"; 
            string patternLocal = "(?<replace></div>.*?sidebar-nav-value\">)";
            
            Regex regex = new Regex(pattern);
            Regex regexLocal = new Regex(patternLocal);
            
            MatchCollection matches = regex.Matches(data);

            if (matches.Count > 0)
                foreach (Match match in matches)
                    Console.WriteLine(Regex.Replace(match.Value, patternLocal, " => "));
            
            Console.ReadKey();
        }
        
        private static string GetDataFromFile()
        {
            return File.ReadAllText($"{Environment.CurrentDirectory}\\htmltext.txt");
        }

        private static string GetDataFromUrl(string url)
        {
            using (WebClient client = new WebClient())
            {
                return client.DownloadString(url);
            }
        }
    }
}